const nodemailer = require('nodemailer');
const amqp = require('amqplib/callback_api');

const transporter = nodemailer.createTransport( {
    service: "hotmail",
    auth: {
        user: "testpweb123@outlook.com",
        pass: "mysafepass12"
    }
});

amqp.connect('amqp://rabbitmq', (err, connection) => {
    if (err) {
        throw err;
    }
    connection.createChannel((err, channel) => {
        if (err) {
            throw err;
        }
        let queueName = 'forgotpassqueue';
        channel.assertQueue(queueName, {
            durable: false
        });
        channel.consume(queueName, (message) => {
            const messageString = message.content.toString();
            messageLst = messageString.split(', ');
            const options = {
                from: "testpweb123@outlook.com",
                to: messageLst[0],
                subject: "Connectr password retrieval",
                text: "Dear " + messageLst[1] + "\n" 
                + "Your password on Connectr is:" + messageLst[2]
            }
            transporter.sendMail(options, function(err, info) {
                if (err) {
                    console.log(err);
                    return;
                }
                console.log("Sent "+ info.response);
            }
        ); 
        })
    })
})
