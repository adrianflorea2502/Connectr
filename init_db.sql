create table USERS (
firstname Varchar(50) NOT NULL,
lastname Varchar(50) NOT NULL,
passwd Varchar(50) NOT NULL,
email varchar(50) NOT NULL,
phone varchar(12) NOT NULL,
country varchar(12) NOT NULL,
birthday varchar(25) NOT NULL,
family varchar(100) NOT NULL,
createdBio Integer NOT NULL);
ALTER TABLE USERS add CONSTRAINT UNIQUE_email UNIQUE (email);
CREATE TABLE DESCRIPTION (
	email varchar(50) UNIQUE NOT NULL,
    aboutMe varchar(500) NOT NULL,
    relatives varchar(200) NOT NULL
);