import {Link} from 'react-router-dom';
import React from 'react';
import { useState } from 'react';
import logo from "../Blue_Pink_Minimalist_Simple_Modern_Icon_Design_Tour_and_Travel_Logo.png";
import axios from 'axios';
const amqp = require('amqplib/callback_api');



const sendMessage = (email) => {
    axios.get("/users/credentials",
        {
            params: {
                email : email
            }}).then((response) => {
                alert(response.data);
            }).catch((err) => {
                console.log(err);
                alert('Email address not found!');
            });
}

export default

function ForgotPasswordPage(){
    const [email, setEmail] = useState('');
    return (
        <div className="ForgotPass">
             <img style={{height: 70, width: 70, position: 'absolute', top: 8, left: 16}} src={logo} alt="Logo"></img>
            <h1>CHANGE PASSWORD</h1>
            <form>
                <div className='container'>
                <input style={{height: 30, width: 300}} placeholder="Enter email here" type="text" name="email" onChange={function(e) {setEmail(e.target.value)}} />
                <br></br>
                <button type='button' onClick={function() {
                    sendMessage(email);
                }}> Send password email</button>
                <br></br>
                <Link to="/">Back to LogIn</Link>
                </div> 
            </form>
            
        </div>
    );
}